package com.pikecape.spingbooterrorhandling.service;

import com.pikecape.spingbooterrorhandling.dao.DuckDao;
import com.pikecape.spingbooterrorhandling.model.Duck;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DuckService.
 * 
 * @author Mika J. Korpela
 */
@Service
public class DuckService
{
    @Autowired
    private DuckDao duckDao;
    
    /**
     * Merge.
     * 
     * @param id
     * @param duck
     * @return Duck
     */
    public Duck merge(UUID id, Duck duck) {
        duck.setId(id);
        duck.setAge(duckDao.getAge());
        return duck;
    }
}
