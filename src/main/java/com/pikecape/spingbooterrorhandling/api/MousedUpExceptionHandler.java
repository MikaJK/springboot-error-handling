package com.pikecape.spingbooterrorhandling.api;

import com.pikecape.spingbooterrorhandling.exception.MousedUpException;
import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * MousedUpExceptionHandler.
 * 
 * @author Mika J. Korpela
 */
@ControllerAdvice
public class MousedUpExceptionHandler
{
    @ExceptionHandler(value = {MousedUpException.class})
    public ResponseEntity<ErrorResponse> handleMouseException(MousedUpException mousedUpException)
    {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setError(mousedUpException.getMessage());
        errorResponse.setStatus(HttpStatus.FORBIDDEN.value());

        return new ResponseEntity<>(errorResponse, HttpStatus.FORBIDDEN);
    }
}
