package com.pikecape.spingbooterrorhandling.api;

import com.pikecape.spingbooterrorhandling.exception.DuckedUpException;
import com.pikecape.spingbooterrorhandling.exception.MousedUpException;
import com.pikecape.spingbooterrorhandling.model.Duck;
import com.pikecape.spingbooterrorhandling.service.DuckService;
import java.time.LocalDateTime;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * ErrorHandlingController.
 *
 * @author Mika J. Korpela
 */
@RequestMapping("api/v1/duck")
@RestController
public class ErrorHandlingController
{
    @Autowired
    private DuckService duckService;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception exception)
    {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setError(exception.getMessage());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(DuckedUpException.class)
    public ResponseEntity<ErrorResponse> duckedUpExceptionHandler(DuckedUpException duckedUpException)
    {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setError(duckedUpException.getMessage());
        errorResponse.setStatus(HttpStatus.NOT_ACCEPTABLE.value());

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_ACCEPTABLE);
    }

    @RequestMapping(path = "{id}", method = RequestMethod.PUT, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Duck> PutDucks(@PathVariable("id") UUID id, @RequestBody Duck duck) throws Exception
    {
        if (duck.getFirstName().equals("Duey")) {
            throw new Exception("Just an exception!");
        } else if (duck.getFirstName().equals("Huey")) {
            throw new DuckedUpException("All ducked up!");
        } else if (duck.getFirstName().equals("Mickey")) {
            throw new MousedUpException("It's a mouse not a duck!");
        } else {
            duck = duckService.merge(id, duck);
            return new ResponseEntity<>(duck, HttpStatus.OK);
        }
    }
}
