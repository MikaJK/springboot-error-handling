package com.pikecape.spingbooterrorhandling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringbootErrorHandlingApplication.
 * 
 * @author Mika J. Korpela
 */
@SpringBootApplication
public class SpringbootErrorHandlingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootErrorHandlingApplication.class, args);
    }
}