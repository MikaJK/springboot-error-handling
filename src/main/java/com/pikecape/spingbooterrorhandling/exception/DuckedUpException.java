package com.pikecape.spingbooterrorhandling.exception;

/**
 * DuckedUpException.
 * 
 * @author Mika J. Korpela
 */
public class DuckedUpException extends Exception
{
    public DuckedUpException()
    {
    }

    public DuckedUpException(String message)
    {
        super(message);
    }
}
