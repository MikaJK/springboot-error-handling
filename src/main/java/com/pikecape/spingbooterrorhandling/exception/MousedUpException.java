package com.pikecape.spingbooterrorhandling.exception;

/**
 * MousedUpException.
 * 
 * @author Mika J. Korpela
 */
public class MousedUpException extends RuntimeException
{
    public MousedUpException()
    {
    }

    public MousedUpException(String message)
    {
        super(message);
    }
}
