package com.pikecape.spingbooterrorhandling.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Duck
{
    // attributes.
    @JsonProperty("id")
    private UUID id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("age")
    private int age;

    /**
     * Constructor.
     * 
     */
    public Duck()
    {
    }

    /**
     * Constructor.
     * 
     * @param id
     * @param firstName
     * @param lastName
     * @param age 
     */
    public Duck(UUID id, String firstName, String lastName, int age)
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    // getters and setters.
    public UUID getId()
    {
        return id;
    }

    public void setId(UUID id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }
}
