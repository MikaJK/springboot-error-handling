package com.pikecape.spingbooterrorhandling.dao;

import org.springframework.stereotype.Component;

/**
 * DuckDao.
 * 
 * @author Mika J. Korpela
 */
@Component
public class DuckDao
{
    /**
     * Get age.
     * 
     * @return int
     */
    public int getAge() {
        return 11;
    }
}
